from django.apps import AppConfig


class FilesConfig(AppConfig):
    name = 'files'
    verbose_name = 'Documentos'

    def ready(self):
        import files.signals
