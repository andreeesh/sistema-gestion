from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from .managers import CustomUserManager

class IvaCondition(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Condición IVA'
        verbose_name_plural = 'Condiciones IVA'
        ordering = ['name']

    def __str__(self):
        return self.name

class Enterprise(models.Model):
    name = models.CharField(max_length=100, verbose_name="Razón Social")
    address = models.CharField(max_length=100, verbose_name="Dirección")
    cuit = models.CharField(max_length=100, verbose_name="CUIT")
    iva_condition = models.ForeignKey(IvaCondition, verbose_name="Condición IVA", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'
        ordering = ['name']

    def __str__(self):
        return self.name

class GenericType(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Tipo genérico'
        verbose_name_plural = 'Tipos genéricos'

    def __str__(self):
        return self.name

class MissionVision(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='mvp', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Misión, Vision y Política de SeHL'
        verbose_name_plural = 'Misiónes, Visiones y Políticas de SeHL'
        ordering = ['name']

    def __str__(self):
        return self.name

class GeneralSurvey(models.Model):
    year = models.CharField(max_length=100, verbose_name="Año")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='rgrl', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Relevamiento General de Riesgos Laborales'
        verbose_name_plural = 'Relevamientos Generales de Riesgos Laborales'
        ordering = ['year']

    def __str__(self):
        return self.year

class AnnualTrainingPlan(models.Model):
    topic = models.CharField(max_length=100, verbose_name="Tema")
    year = models.CharField(max_length=100, verbose_name="Año")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='atp', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Plan de Capacitación Anual'
        verbose_name_plural = 'Planes de Capacitación Anual'
        ordering = ['topic']

    def __str__(self):
        return self.topic

class WorkProcedure(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    type = models.ForeignKey(GenericType, verbose_name="Tipo", on_delete=models.CASCADE)
    year = models.CharField(max_length=100, verbose_name="Año")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='wp', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Procedimientos de Trabajo y Normas de Trabajo Seguro'
        verbose_name_plural = 'Procedimientos de Trabajo y Normas de Trabajo Seguro'
        ordering = ['name']

    def __str__(self):
        return self.name

class SelfProtectionSystem(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    type = models.ForeignKey(GenericType, verbose_name="Tipo", on_delete=models.CASCADE)
    year = models.CharField(max_length=100, verbose_name="Año")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='wp', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Sistema de Autoprotección'
        verbose_name_plural = 'Sistemas de Autoprotección'
        ordering = ['name']

    def __str__(self):
        return self.name

class PersonalProtectionItems(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    type = models.ForeignKey(GenericType, verbose_name="Tipo", on_delete=models.CASCADE)
    year = models.CharField(max_length=100, verbose_name="Año")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='wp', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Elemento de Protección Personal'
        verbose_name_plural = 'Elementos de Protección Personal'
        ordering = ['name']

    def __str__(self):
        return self.name

class SafetyInspections(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nombre")
    enterprise = models.ForeignKey(Enterprise, verbose_name="Empresa", on_delete=models.CASCADE)
    file = models.FileField(upload_to='wp', null=True, verbose_name="Archivo")
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name="Creado")
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name="Actualizado")

    class Meta:
        verbose_name = 'Inspección de Seguridad'
        verbose_name_plural = 'Inspecciones de Seguridad'
        ordering = ['name']

    def __str__(self):
        return self.name

class CustomUser(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(verbose_name='nombre', max_length=20)
    last_name = models.CharField(verbose_name='apellido', max_length=20)
    phone = models.CharField(unique=True, verbose_name='telefono', max_length=10)
    email = models.EmailField(unique=True, verbose_name='email', null=True, blank=True, default=None)
    is_staff = models.BooleanField(default=True, verbose_name='Acceso al sistema')
    is_active = models.BooleanField(default=True, verbose_name='Usuario activo')
    enterprise = models.ForeignKey(Enterprise, blank=True, verbose_name='Empresa', null=True, on_delete=models.CASCADE)

    USERNAME_FIELD = ('phone')

    objects = CustomUserManager()

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __str__(self):
        return self.phone
