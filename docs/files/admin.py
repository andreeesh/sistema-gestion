from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe
from .forms import CustomUserCreationForm, CustomUserChangeForm

from django import forms
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class EnterpriseAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'cuit')
    list_display_links = ('name',)
    search_fields = ('name', 'address', 'cuit')
    list_per_page = 8

class GenericTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)
    list_per_page = 8

class MissionVisionAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise',)
        else:
            return ()

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'file_link')
        else:
            return ('file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name',)
        else:
            return ()

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class GeneralSurveyAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'year')
        else:
            return ('year',)

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'year', 'file_link')
        else:
            return ('year', 'file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name', 'year')
        else:
            return ('year')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class AnnualTrainingPlanAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'topic', 'year')
        else:
            return ('topic', 'year',)

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'topic', 'year', 'file_link')
        else:
            return ('topic', 'year', 'file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name', 'topic', 'year')
        else:
            return ('topic', 'year')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class WorkProcedureAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'type', 'year')
        else:
            return ('type', 'year',)

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'type', 'name', 'year', 'file_link')
        else:
            return ('type', 'name', 'year', 'file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name', 'type__name', 'name', 'year')
        else:
            return ('type__name', 'name', 'year')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class SelfProtectionSystemAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'type', 'year')
        else:
            return ('type', 'year',)

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'type', 'name', 'year', 'file_link')
        else:
            return ('type', 'name', 'year', 'file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name', 'type__name', 'name', 'year')
        else:
            return ('type__name', 'name', 'year')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class PersonalProtectionItemsAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'type', 'year')
        else:
            return ('type', 'year',)

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'type', 'name', 'year', 'file_link')
        else:
            return ('type', 'name', 'year', 'file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name', 'type__name', 'name', 'year')
        else:
            return ('type__name', 'name', 'year')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class SafetyInspectionsAdmin(admin.ModelAdmin):
    readonly_fields = ('file_link',)
    list_per_page = 8

    def get_list_filter(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'name')
        else:
            return ()

    def get_list_display(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise', 'name', 'file_link')
        else:
            return ('name', 'file_link',)

    def get_search_fields(self, request):
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return ('enterprise__name', 'name')
        else:
            return ('name')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.groups.filter(name='Administrador').exists():
            return qs
        return qs.filter(enterprise=request.user.enterprise_id)

    def file_link(self, obj):
        if obj.file:
            return mark_safe('<a target="_blank" href="%s">Ver archivo</a>' % (obj.file.url,))
        else:
            return 'No disponible'

    file_link.short_description = 'Archivo'

class CustomUserAdmin(UserAdmin):
    readonly_fields = ('whatsapp_link',)
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('name', 'last_name', 'phone', 'email', 'is_staff', 'is_active', 'enterprise', 'whatsapp_link')
    list_display_links = ('phone',)
    list_filter = ('enterprise',)
    search_fields = ('name', 'last_name', 'phone', 'email')
    ordering = ('name', 'last_name', 'phone', 'email')

    fieldsets = (
        (None, {'fields': ('name', 'last_name', 'enterprise', 'phone', 'email', 'password', 'is_active')}),
        ('Permisos', {'fields': ('groups',)}),
    )

    add_fieldsets = (
        (None, {'fields': ('name', 'last_name', 'enterprise', 'phone', 'email', 'password1', 'password2')}),
        ('Permisos', {'fields': ('groups',)}),
    )

    def whatsapp_link(self, obj):
        return mark_safe('<a target="_blank" href="https://web.whatsapp.com/send?phone=+549'+obj.phone+'&text=🗣🗣️ *AFL Seguridad e Higiene Laboral* les da la bienvenida a la nueva plataformo donde podra contar con toda la documentacion al instante 📄📄📄 en cualquier momento. *LOS PASOS* 👣 *SON SIMPLES*: Ingresando al link se accede a la pagina la cual en la margen superior derecho posee el enlace donde ud. debera logearse con el USUARIO q le enviaremos; una vez ingresado, *PODRAS VER Y DESCARGAR* Disposiciones, informes de simulacros y demas cuestiones.">Enviar</a>')

    whatsapp_link.short_description = 'Bienvenida'

admin.site.register(Enterprise, EnterpriseAdmin)
admin.site.register(MissionVision, MissionVisionAdmin)
admin.site.register(GeneralSurvey, GeneralSurveyAdmin)
admin.site.register(AnnualTrainingPlan, AnnualTrainingPlanAdmin)
admin.site.register(IvaCondition)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(GenericType, GenericTypeAdmin)
admin.site.register(WorkProcedure, WorkProcedureAdmin)
admin.site.register(SelfProtectionSystem, SelfProtectionSystemAdmin)
admin.site.register(PersonalProtectionItems, PersonalProtectionItemsAdmin)
admin.site.register(SafetyInspections, SafetyInspectionsAdmin)
