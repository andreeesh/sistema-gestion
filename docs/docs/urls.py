from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings

admin.site.index_title = 'Escritorio'
admin.site.site_header = 'AFL Higiene y Seguridad'
admin.site.site_title = 'AFL Higiene y Seguridad'

urlpatterns = i18n_patterns (
    path('', admin.site.urls),
    prefix_default_language=False,
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
