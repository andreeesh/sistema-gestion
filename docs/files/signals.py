from .models import *
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

# @receiver(post_save, sender=CustomUser)
# def send_welcome_email(sender, instance, created, **kwargs):
#     if (created):
#         if (instance.email):
#             subject = 'Bienvenido a AFLHYS'
#             html_message = render_to_string('emails/welcome_email.html', {'context': 'values'})
#             plain_message = strip_tags(html_message)
#             from_email = 'Intranet AFLHYS <no-responder@cspfa.org.ar>'
#             to = instance.email
#             send_mail(subject, plain_message, from_email, [to], html_message=html_message)
